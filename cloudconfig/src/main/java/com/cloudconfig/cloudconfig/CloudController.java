package com.cloudconfig.cloudconfig;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CloudController {
	
	@Value("${connectionURL}")
	private String connectionURL;
	
	@GetMapping("/getData")
	public String getData() {
		return connectionURL;
	}

}
